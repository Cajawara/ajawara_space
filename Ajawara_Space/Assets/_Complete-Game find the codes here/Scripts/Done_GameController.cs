﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class Done_GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;
    private int score;

    public Game_Dicitionary_Manager dictionaryManager;
    public Game_Ui_Manager uiManager;
    void Start()
    {
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
        uiManager.wordInputField.ActivateInputField();
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                GameObject hazardsinstance = Instantiate(hazard, spawnPosition, spawnRotation) as GameObject;
               HazardWordController h = hazardsinstance.GetComponent<HazardWordController>();
                h.myWord = dictionaryManager.getNewWord();
                h.GenerateTextObject();
                dictionaryManager.setLiveWord(h.myWord);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void TriggerWordEntry()
    {
        string inputword = uiManager.wordInputField.text;
        Debug.Log("player has entered: " + inputword);
        if (!dictionaryManager.liveWords.Contains(inputword))
        {
            Debug.Log(inputword+" is NOT a live word");
            //hazard
        }
        else {
            Debug.Log(inputword + " is a live word");
            HazardWordController[] hazardsWords = GameObject.FindObjectsOfType<HazardWordController>();
            foreach (HazardWordController h in hazardsWords)
            {
                if (h.myWord == inputword)
                {
                    Debug.Log(h.gameObject.name + " is  a valid word target");
                    Done_DestroyByContact d = h.gameObject.GetComponent<Done_DestroyByContact>();
                    d.DestroyBehavior();
                }
                else Debug.Log(h.gameObject.name + " is NOT a valid word target");
            }

        }
       
        uiManager.wordInputField.text = "";
        uiManager.wordInputField.ActivateInputField();
    }
    

    public void TriggerWordKill(GameObject targetHazard)
    {
        HazardWordController h = targetHazard.GetComponent<HazardWordController>();
        dictionaryManager.setDeadWord(h.myWord);
    }
}