﻿using UnityEngine;
using System.Collections;

public class Done_DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private Done_GameController gameController;

    public bool shieldon = false;

    void Start ()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <Done_GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Boundary" || other.tag == "Enemy")
		{
			return;
		}
        if (other.tag == "Shield")
        {
            shieldon = true;
            GetComponent<AudioSource>().Play();
        }
		

		if (other.tag == "Player")
		{
			
			gameController.GameOver();
		}
		
		
        
		Destroy (other.gameObject);
        DestroyBehavior();
	}
    public void DestroyBehavior()
    {
        gameController.TriggerWordKill(gameObject);

        if (Instantiate(explosion, transform.position, transform.rotation);
     
        gameController.AddScore(scoreValue);
        Destroy(gameObject);
    }
}