﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Dicitionary_Manager : MonoBehaviour {

    public List<string> allWords;
    public List<string> liveWords;
    public List<string> deadWords;
    private void Awake()
    {
        deadWords = allWords;
    }
    public string getNewWord() {

        int randomWord = Random.Range(0,deadWords.Count-1);
        return deadWords[randomWord];  
    }
    public void setLiveWord(string s) {
        deadWords.Remove(s);
        liveWords.Add(s);
       
    }
    public void setDeadWord(string s)
    {
        liveWords.Remove(s);
        deadWords.Add(s);

    }
}


