﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardWordController : MonoBehaviour {
    public string myWord;
    public GameObject textPrefab;
    public void GenerateTextObject()
    {
        GameObject g = Instantiate(textPrefab, this.transform);
        if (g.GetComponentInChildren<TextMesh>())
        g.GetComponentInChildren<TextMesh>().text = myWord;
    }
}
