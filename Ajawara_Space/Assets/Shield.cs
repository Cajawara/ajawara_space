﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {
    public bool shieldon = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            shieldon = true;
            GetComponent<AudioSource>().Play();
        }
        if (other.tag == "Boundary" || other.tag == "Enemy")
        {
            return;
        }
    }
}
